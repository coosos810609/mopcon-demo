#!/bin/sh
gcloud auth activate-service-account mopcon@halogen-device-252213.iam.gserviceaccount.com --key-file=/k8s/cert.json --project=mopcon-demo
gcloud config set project halogen-device-252213
gcloud config set compute/zone asia-east1-c
gcloud components install kubectl -q
gcloud container clusters get-credentials mopcon-demo
kubectl apply -f deployment.yaml --record
